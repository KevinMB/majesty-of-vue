const app =new Vue({
    el: "#app",
    data: {
        titulo:"Holz con este Framework",
        calificacion:[
            {nombre:"curso1", nota:09},
            {nombre:"curso2", nota:16},
            {nombre:"curso3", nota:20}
        ],
        nuevaNota: "",
        totalNotas: 0
        
    },

    methods:{
        agregarNota(){
            this.calificacion.push({
                nombre:this.nuevaNota, nota: 20
         });
         this.nuevaNota='';
        }

    },

    computed:   {
        sumarNotas(){
            this.totalNotas=0;
            for( not of this.calificacion){
                this.totalNotas = this.totalNotas + not.nota;
            }
            return this.totalNotas;
        }
    }
    
})