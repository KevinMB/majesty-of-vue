
const submissionComponent = {
    template:
        `<div style="display: flex; width: 100%">
            <figure class="media-left">
                <img class="image is-64x64"
                v-bind:src="submission.image">
            </figure>
            <div class="media-content">
                <div class="content">
                    <p>
                    <strong>
                    <a v-bind:href="submission.url" class="has-text-info">
                    {{ submission.title }}
                     </a>
                     <span class="tag is-small">#{{ submission.id }}</span>
                    </strong>
                    <br>
                    {{ submission.description }}
                    <br>
                <small class="is-size-7">
                    Submitted by:   
                  <span class="icon">
                    <i :class="submission.avatar"></i>
                  </span>
                </small>
                    </p>
                </div>
            </div>
            <div class="media-right">
                <span class="icon is-small" @click="upvote(submission.id)">
                    <i class="fa fa-chevron-up"></i>
                 <strong class="has-text-info">{{ submission.votes }}</strong>
                </span>
            </div>
        </div>`,

    props: ['submission', 'submissions'],
    methods: {
        upvote(submissionId) {
            const submission = this.submissions.find(
                submission => submission.id === submissionId
            );
            submission.votes++;
        }
    }

};



const app = new Vue({
    el: "#app",
    data: () => {
        return {
            submissions: [
                {
                    id: "1",
                    image: 'https://bulma.io/images/placeholders/128x128.png',
                    url: '#',
                    title: 'Yelow',
                    description: 'On-demand sand castle construction expertise.',
                    avatar: 'fas fa-home',
                    votes: 18
                },
                {
                    id: "5",
                    image: 'https://bulma.io/images/placeholders/128x128.png',
                    url: '#',
                    title: 'White',
                    description: 'On-demand sand castle construction expertise.',
                    avatar: 'fas fa-home',
                    votes: 10
                },
                {
                    id: "2",
                    image: 'https://bulma.io/images/placeholders/128x128.png',
                    url: '#',
                    title: 'Blue',
                    description: 'On-demand sand castle construction expertise.',
                    avatar: 'fas fa-home',
                    votes: 13
                },
                {
                    id: "3",
                    image: 'https://bulma.io/images/placeholders/128x128.png',
                    url: '#',
                    title: 'Black',
                    description: 'On-demand sand castle construction expertise.',
                    avatar: 'fas fa-home',
                    votes: 14
                },
                {
                    id: "4",
                    image: 'https://bulma.io/images/placeholders/128x128.png',
                    url: '#',
                    title: 'Orange',
                    description: 'On-demand sand castle construction expertise.',
                    avatar: 'fas fa-home',
                    votes: 19
                }

            ],
            titleTemp: '',
            title: 'UpVote',
            newSubmission: {
                id: "1",
                image: 'https://bulma.io/images/placeholders/128x128.png',
                url: '#',
                title: 'Yelow',
                description: 'On-demand sand castle construction expertise.',
                avatar: 'fas fa-home',
                votes: 18
            },
        }
    },
    computed: {
        sortedSubmissions() {
            return this.submissions.sort((a, b) => {
                return b.votes - a.votes;
            })
        }
    },
    methods: {
        upvote(submissionId) {
            const submission = this.submissions.find(
                submission => submission.id === submissionId
            );
            submission.votes++;
        },
        saveTitle(newTitle) {
            this.title = newTitle;
        },
        onSubmit(event) {
            /// console.log(event.target.title.value)
            this.submissions.push(this.newSubmission);
        },
        test() {
            console.log('He hecho doble click')
        }
    },

    components: {
        'submission-component': submissionComponent
    }


})